package pages;

import com.codeborne.selenide.ElementsCollection;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$$;

public class ProductsPage {

    ElementsCollection elementsOfProductList = $$(By.xpath("//uc-product-list/product-card"));

    public String getPriceOfFirstElementInProductList() {
        return elementsOfProductList.get(elementsOfProductList.size() - 1).getAttribute("product-price");
    }
}
