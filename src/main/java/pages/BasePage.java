package pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.codeborne.selenide.Selenide.$$;

public class BasePage {

    WebElement allowCookiesButton = (WebElement) ((JavascriptExecutor) WebDriverRunner.getWebDriver())
            .executeScript("return document.querySelector('uc-cookie-notification').shadowRoot.querySelector('.submit-button-inner')");

    WebElement nextCarouselButton = (WebElement) ((JavascriptExecutor) WebDriverRunner.getWebDriver())
            .executeScript("return document.querySelector('promo-banners-carusel').shadowRoot.querySelector('uc-button.nav-button.next.js-next-button')");

    ElementsCollection elementsInCarousel = $$(By.xpath("//promo-banners-carusel/a"));

    public void clickOnAllowCookies() {
        allowCookiesButton.click();
    }

    public void clickOnNextProductsButtonWhileActive() {
        WebDriverWait wait = new WebDriverWait(WebDriverRunner.getWebDriver(), 5);

        try {
            while (nextCarouselButton.isDisplayed()) {
                wait.until(ExpectedConditions.elementToBeClickable(nextCarouselButton));
                nextCarouselButton.click();
            }
        } catch (NoSuchElementException e) {

        }
    }

    public void clickOnLatestElementInCarousel(){
        elementsInCarousel.get(elementsInCarousel.size()-1).click();
    }
}