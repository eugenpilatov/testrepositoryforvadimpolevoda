package autotests;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import configurations.ConfProperties;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pages.BasePage;
import pages.ProductsPage;

import static com.codeborne.selenide.Selenide.open;

public class BasePageAndBlogTest {
    BasePage basePage;
    ProductsPage productsPage;

    /**
     * осуществление первоначальной настройки
     */
    @BeforeEach
    public void setup() {
        Configuration.browser = "chrome";
        Configuration.timeout = 6000;
        open(ConfProperties.getConfProperties().getProperty("BasePage"));

    }

    @AfterEach
    public void exit() {
        if (WebDriverRunner.getWebDriver() != null) {
            Selenide.closeWebDriver();
        }
    }

    @Test
    public void adminPageAndAddEntryTest() throws InterruptedException {
        basePage = new BasePage();
        productsPage = new ProductsPage();

        basePage.clickOnAllowCookies();
        basePage.clickOnNextProductsButtonWhileActive();
        basePage.clickOnLatestElementInCarousel();

        System.out.println(productsPage.getPriceOfFirstElementInProductList());
    }
}
